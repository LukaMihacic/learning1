package com.luka.learning;

import com.google.gson.annotations.SerializedName;

public class Contact {
    @SerializedName("image")
    private String image;

    @SerializedName("id")
    private String company;

    @SerializedName("email")
    private String email;


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}

