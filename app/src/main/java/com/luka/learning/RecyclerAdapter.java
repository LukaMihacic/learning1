package com.luka.learning;

import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.MyViewHolder> {

     List <Contact> contacts;
     private Context context;
     OnSuccessListener listener;

    public RecyclerAdapter(List<Contact> contacts, Context context, OnSuccessListener listener){
        this.contacts = contacts;
        this.context = context;
        this.listener = listener;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        Glide.with(context).load(contacts.get(position).getImage()).into(holder.Picture);
        holder.Company.setText(contacts.get(position).getCompany());
        holder.Email.setText(contacts.get(position).getEmail());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "You clicked on " + contacts.get(position).getCompany() + " item",
                        Toast.LENGTH_SHORT).show();
                listener.onSuccess();
            }
        });
    }

    @Override
    public int getItemCount() {
        return contacts.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView Company, Email, Id;
        ImageView Picture;

        public MyViewHolder(View itemView) {
            super(itemView);
            Picture = itemView.findViewById(R.id.ivProfile);
            Company = itemView.findViewById(R.id.tvCompany);
            Email = itemView.findViewById(R.id.tvEmail);
            Id = itemView.findViewById(R.id.tvId);
        }
    }
}
